﻿$groupmembers = get-adgroupmember -identity 'CRO All' | Select distinguishedName


#$groupmembers.distinguishedName | Add-Content "CRO_All.txt"

Function GetUserProfile($User_DN){
    $details_of_user=Get-ADUser -Identity $User_DN -Properties sAMAccountName,Title,directReports,CN,distinguishedName,Manager
    return $details_of_user


}

Function GetManagerInfo($Manager_DN){
    $details_of_manager=Get-ADUser -Identity $Manager_DN -Properties sAMAccountName,Title,directReports,CN,distinguishedName,Manager
    return $details_of_manager

}


function haveDirectReports($who_is_DN){
    $direct_reports=get-aduser -Identity $who_is_DN -Properties *
   if(($direct_reports.directReports -ne $null)){
        return $True
    }else{
        return $False
    }

}

$UserArray=@()

$index=0
foreach($member in $groupmembers){
    $member_profile = GetUserProfile $member.distinguishedName
    $have_directreports =haveDirectReports $member.distinguishedName

   
    if($User.hasDirectReports){
        
        foreach($i_member in $member_profile.directReports){
             $User = New-Object -TypeName PSObject 
           
            $User | Add-Member -Name 'UID' -MemberType NoteProperty -Value $index
            $User | Add-Member -Name 'UserDN' -MemberType Noteproperty -Value $member.distinguishedName
            $User | Add-Member -Name 'UserName' -MemberType Noteproperty -Value $member_profile.CN
            $User | Add-Member -Name 'Title' -MemberType Noteproperty -Value $member_profile.Title
            $User | Add-Member -Name 'Manager' NoteProperty -Value $(GetManagerInfo $member_profile.Manager).CN
            $User | Add-Member -Name 'hasDirectReports' Noteproperty -Value  $have_directreports
            $User | Add-Member -Name 'DirectReport' Noteproperty -Value  $i_member
            

            $UserArray+=$User
            $index+=1
            }


        
 
    }else{
            $User = New-Object -TypeName PSObject 
            $User | Add-Member -Name 'UID' -MemberType NoteProperty -Value $index
            $User | Add-Member -Name 'UserDN' -MemberType Noteproperty -Value $member.distinguishedName
            $User | Add-Member -Name 'UserName' -MemberType Noteproperty -Value $member_profile.CN
            $User | Add-Member -Name 'Title' -MemberType Noteproperty -Value $member_profile.Title
            $User | Add-Member -Name 'Manager' NoteProperty -Value $(GetManagerInfo $member_profile.Manager).CN
            $User | Add-Member -Name 'hasDirectReports' Noteproperty -Value  $have_directreports
            $User | Add-Member -Name 'DirectReports' NoteProperty -Value $null
            

            $UserArray+=$User
            $index+=1
    }

    

}



<#
$my=GetManagerInfo 'CN=Lakshmikanth Ganta,OU=Users,OU=Users_And_Groups,DC=S24,DC=local'

$my.CN
#>


$UserArray | ConvertTo-Json